type ToriImage = {
  base_url: string;
  media_id: string;
  path: string;
  width: number;
  height: number;
};

type ToriAd = {
  ad: {
    body: string;
    ad_id: string;
    list_id_code: string;
    company_ad: boolean;
    images: ToriImage[];
    list_price: { currency: 'EUR'; price_value: number; label: string };
    subject: string;
    share_link: string;
    list_time: {
      label: string;
      value: number;
    };
    thumbnail: {
      base_url: string;
      height: number;
      media_id: string;
      path: string;
      width: number;
    };
  };
};

export type ToriResponse = {
  config_etag: string;
  counter_map: { all: number };
  list_ads: ToriAd[];
};
