type HuutonetImage = {
  links: {
    self: string;
    thumbnail: string;
    medium: string;
    original: null;
  };
};

type HuutonetItem = {
  links: {
    self: string;
    category: string;
    alternative: string;
    images: string;
  };
  id: number;
  title: string;
  category: string;
  seller: string;
  sellerId: number;
  currentPrice: number;
  buyNowPrice: number;
  saleMethod: string;
  listTime: string;
  postalCode: string;
  location: string;
  closingTime: string;
  bidderCount: number;
  offerCount: number;
  hasReservePrice: boolean;
  hasReservePriceExceeded: boolean;
  upgrades: [];
  images: HuutonetImage[];
};

export type HuutonetResponse = {
  totalCount: number;
  updated: string;
  links: {
    self: string;
    first: string;
    last: string;
    previous: null;
    next: string;
    gallery: string;
    hits: string;
  };
  items: HuutonetItem[];
};
