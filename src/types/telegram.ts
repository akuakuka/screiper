export type TelegramNotificationMessage = {
  body: string;
  subject: string;
  url: string;
  price: string;
};
