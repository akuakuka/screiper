import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { HuutonetService } from './huutonet.service';

@Module({
  imports: [HttpModule],
  providers: [HuutonetService],
})
export class HuutonetModule {}
