import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import { HuutonetResponse } from 'src/types/huutonet';

@Injectable()
export class HuutonetService {
  constructor(private readonly httpService: HttpService) {}
  private readonly BASEURL = 'https://api.huuto.net/1.1';
  searchKeyword(keyword: string): Promise<AxiosResponse<HuutonetResponse>> {
    return this.httpService.axiosRef.get(
      `${this.BASEURL}/items?words=${keyword}&status=open&sort=newest`,
    );
  }
}
