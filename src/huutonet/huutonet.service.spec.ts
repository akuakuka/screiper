import { Test, TestingModule } from '@nestjs/testing';
import { HuutonetService } from './huutonet.service';

describe('HuutonetService', () => {
  let service: HuutonetService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [HuutonetService],
    }).compile();

    service = module.get<HuutonetService>(HuutonetService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
