export const epochSecondsToMillisecons = (epoch: number) => epoch * 1000;
