import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as TelegramBot from 'node-telegram-bot-api';
import { KeywordsService } from 'src/keywords/keywords.service';

@Injectable()
export class TelegramChatService {
  private readonly bot: TelegramBot;

  private logger = new Logger(TelegramChatService.name);

  constructor(
    private keywordService: KeywordsService,
    private configService: ConfigService,
  ) {
    this.bot = new TelegramBot(
      this.configService.getOrThrow('telegram.token'),
      {
        polling: true,
      },
    );

    this.bot.onText(/\/addkeyword (.+)/, async (msg, match) => {
      this.logger.verbose('Telegram: Getting addKeyword message');
      const keyword = match[1];
      try {
        const newKeyword = await this.keywordService.create(keyword);
        this.sendMessageToChat(
          `Created keyword ${newKeyword.keyword} with id: ${newKeyword.id}`,
        );
      } catch (e) {
        this.sendMessageToChat(`Error creating keyword ${JSON.stringify(e)}`);
      }
    });

    this.bot.onText(/\/removekeyword (.+)/, async (msg, match) => {
      this.logger.verbose('Telegram: Getting removekeyword message');
      const keyword = match[1];
      const found = await this.keywordService.search(keyword);
      if (!found) return this.sendMessageToChat(`No keyword found`);
      await this.keywordService.delete(found.id);
      return this.sendMessageToChat(`Keyword with id ${found.id} deleted`);
    });
  }
  sendMessageToChat = (message: string) => {
    this.bot.sendMessage(
      this.configService.getOrThrow('telegram.chatid'),
      message,
    );
  };

  sendResultMessageToChat = (
    name: string,
    price: string,
    link: string,
    keyword: string,
  ) => {
    const htmlMessage = `<a href="${link}"><b>${keyword}</b>${name} - ${price} </a>`;
    this.bot.sendMessage(
      this.configService.getOrThrow('telegram.chatid'),
      htmlMessage,
      {
        parse_mode: 'HTML',
      },
    );
  };
}
