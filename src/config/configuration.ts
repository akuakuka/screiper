export default () => ({
  port: parseInt(process.env.PORT, 10) || 3000,
  telegram: {
    token: process.env.TELEGRAM_BOT_TOKEN,
    chatid: process.env.TELEGRAM_CHAT_ID,
  },
  database: {
    host: process.env.DB_HOST,
    port: parseInt(process.env.DB_PORT, 10) || 5432,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database_name: process.env.DB_DB_NAME,
  },
});
