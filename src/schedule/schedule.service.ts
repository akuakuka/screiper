import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { HuutonetService } from 'src/huutonet/huutonet.service';
import { KeywordsService } from 'src/keywords/keywords.service';
import { ResultService } from 'src/result/result.service';
import { TelegramChatService } from 'src/telegram/telegram.service';
import { ToriService } from 'src/tori/tori.service';
import { SourceEnum } from 'src/types/enums';
import { epochSecondsToMillisecons } from 'src/utils/utils';

@Injectable()
export class ScheduleService {
  private logger = new Logger(TelegramChatService.name);
  constructor(
    private keywordService: KeywordsService,
    private resultService: ResultService,
    private telegramChatService: TelegramChatService,
    private toriService: ToriService,
    private huutonetService: HuutonetService,
  ) {}

  @Cron(CronExpression.EVERY_5_MINUTES)
  async searchNewHuutonetResults() {
    const keywords = await this.keywordService.findAll();

    this.logger.verbose(`Searching huuto.net for ${keywords.length} keywords!`);

    for (let k = 0; k < keywords.length; k++) {
      const keyword = keywords[k];
      const { data } = await this.huutonetService.searchKeyword(
        keyword.keyword,
      );
      for (let i = 0; i < data.items.length; i++) {
        const item = data.items[i];
        const exits = await this.resultService.search({
          source: SourceEnum.HUUTONET,
          sourceId: item.id.toString(),
        });
        const itemEpochListDate = new Date(item.listTime).getTime();

        if (itemEpochListDate > keyword.addTimeEpoch && exits.length === 0) {
          const newResult = {
            source: SourceEnum.HUUTONET,
            sourceId: item.id.toString(),
            dateEpoch: null,
            url: item.links.alternative,
            image: null,
            price: item.currentPrice.toString(),
            subject: item.title,
            body: '',
            keyword,
          };
          await this.resultService.create({ ...newResult });
        }
      }
    }
  }

  @Cron(CronExpression.EVERY_5_MINUTES)
  async searchNewToriResults() {
    const keywords = await this.keywordService.findAll();
    this.logger.verbose(`Searching TORI.FI for ${keywords.length} keywords!`);

    for (let k = 0; k < keywords.length; k++) {
      const keyword = keywords[k];
      const { data } = await this.toriService.searchKeyword(keyword.keyword);

      for (let i = 0; i < data.list_ads.length; i++) {
        const { ad } = data.list_ads[i];
        const exits = await this.resultService.search({
          source: SourceEnum.TORI,
          sourceId: ad.ad_id,
        });

        const itemEpochListDate = epochSecondsToMillisecons(ad.list_time.value);

        if (itemEpochListDate > keyword.addTimeEpoch && exits.length === 0) {
          const newResult = {
            source: SourceEnum.TORI,
            sourceId: ad.list_id_code,
            dateEpoch: ad.list_time.value,
            url: ad.share_link,
            image: null,
            price: ad.list_price?.label || null,
            subject: ad.subject,
            body: ad.body,
            keyword,
          };
          await this.resultService.create({ ...newResult });
        }
      }
    }
  }

  @Cron(CronExpression.EVERY_30_SECONDS)
  async triggerNotifications() {
    const result = await this.resultService.findFirstToNotify();
    console.log(result);
    if (!result) return;

    this.logger.verbose(`Triggering notifications for  id : ${result.id} `);

    await this.telegramChatService.sendResultMessageToChat(
      result.subject,
      result.price,
      result.url,
      result.keyword.keyword,
    );

    await this.resultService.create({ ...result, notified: true });
  }
}
