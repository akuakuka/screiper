import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KeywordsModule } from './keywords/keywords.module';
import { KeywordsService } from './keywords/keywords.service';

import { KeywordsController } from './keywords/keywords.controller';
import { Keyword } from './keywords/keywords.entity';
import { ScheduleModule } from '@nestjs/schedule';
import { ScheduleService } from './schedule/schedule.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TelegramChatService } from './telegram/telegram.service';
import configuration from './config/configuration';
import { HttpModule } from '@nestjs/axios';
import { ToriModule } from './tori/tori.module';
import { ToriService } from './tori/tori.service';
import { ResultModule } from './result/result.module';
import { ResultService } from './result/result.service';
import { ResultController } from './result/result.controller';
import { Result } from './result/result.entity';
import { HuutonetModule } from './huutonet/huutonet.module';
import { HuutonetService } from './huutonet/huutonet.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: '.env',
      load: [configuration],
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        type: 'postgres',
        host: configService.getOrThrow('database.host'),
        port: configService.getOrThrow('database.port'),
        username: configService.getOrThrow('database.username'),
        password: configService.getOrThrow('database.password'),
        database: configService.getOrThrow('database.database_name'),
        entities: [Keyword, Result],
        synchronize: true,
        autoLoadEntities: true,
      }),
      inject: [ConfigService],
    }),
    ScheduleModule.forRoot(),
    KeywordsModule,
    HttpModule,
    ToriModule,
    ResultModule,
    HuutonetModule,
  ],
  controllers: [KeywordsController, ResultController],
  providers: [
    KeywordsService,
    ResultService,
    ScheduleService,
    TelegramChatService,
    ToriService,
    HuutonetService,
  ],
})
export class AppModule {}
