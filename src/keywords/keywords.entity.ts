import { Result } from 'src/result/result.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  OneToMany,
} from 'typeorm';

@Entity()
export class Keyword extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 500, unique: true })
  keyword: string;

  @Column({ type: 'bigint' })
  addTimeEpoch: number;

  @OneToMany(() => Result, (result) => result.keyword)
  results: Result[];
}
