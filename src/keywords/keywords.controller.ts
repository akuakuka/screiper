import { Controller, Get, Post } from '@nestjs/common';
import { KeywordsService } from './keywords.service';
import { Keyword } from './keywords.entity';

@Controller('keywords')
export class KeywordsController {
  constructor(private keywordService: KeywordsService) {}

  @Get()
  async findAll(): Promise<Keyword[]> {
    return this.keywordService.findAll();
  }
  @Post()
  async create(keyword: string): Promise<Keyword> {
    return this.keywordService.create(keyword);
  }
}
