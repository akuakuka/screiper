import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Keyword } from './keywords.entity';
import { Repository } from 'typeorm';

@Injectable()
export class KeywordsService {
  constructor(
    @InjectRepository(Keyword)
    private keywordRepository: Repository<Keyword>,
  ) {}
  findAll() {
    return this.keywordRepository.find();
  }
  find(id: number) {
    return this.keywordRepository.findOne({ where: { id } });
  }
  search(term: string) {
    return this.keywordRepository.findOne({ where: { keyword: term } });
  }
  async create(keyword: string) {
    const addTimeEpoch = Date.now();
    console.log(addTimeEpoch);
    const found = await this.keywordRepository.findOne({ where: { keyword } });
    if (found?.id) {
      return await Promise.reject('Keyword must be unique');
    }
    return this.keywordRepository.save({ keyword, addTimeEpoch });
  }
  delete(id: number) {
    return this.keywordRepository.delete(id);
  }
}
