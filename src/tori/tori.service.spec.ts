import { Test, TestingModule } from '@nestjs/testing';
import { ToriService } from './tori.service';

describe('ToriService', () => {
  let service: ToriService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ToriService],
    }).compile();

    service = module.get<ToriService>(ToriService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
