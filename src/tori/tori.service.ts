import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import { ToriResponse } from 'src/types/tori';

@Injectable()
export class ToriService {
  constructor(private readonly httpService: HttpService) {}
  private readonly BASEURL = 'https://api.tori.fi/api/v1.2/public/';
  searchKeyword(keyword: string): Promise<AxiosResponse<ToriResponse>> {
    return this.httpService.axiosRef.get(
      `${this.BASEURL}/ads?q=${keyword}&ad_type=s`,
    );
  }
}
