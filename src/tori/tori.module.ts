import { Module } from '@nestjs/common';
import { ToriService } from './tori.service';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [HttpModule],
  providers: [ToriService],
})
export class ToriModule {}
