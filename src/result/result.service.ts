import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Result } from './result.entity';
import { SourceEnum } from 'src/types/enums';

@Injectable()
export class ResultService {
  constructor(
    @InjectRepository(Result)
    private resultRepository: Repository<Result>,
  ) {}
  findAll() {
    return this.resultRepository.find();
  }
  findFirstToNotify() {
    return this.resultRepository.findOne({
      where: { notified: false },
      relations: { keyword: true },
    });
  }
  search(searchTerms: { source: SourceEnum; sourceId: string }) {
    return this.resultRepository.find({ where: { ...searchTerms } });
  }
  create(result: Partial<Result>) {
    return this.resultRepository.save({ ...result });
  }
}
