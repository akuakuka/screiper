import { Controller, Get } from '@nestjs/common';

import { ResultService } from './result.service';
import { Result } from './result.entity';

@Controller('results')
export class ResultController {
  constructor(private resultService: ResultService) {}
  @Get()
  async findAll(): Promise<Result[]> {
    return this.resultService.findAll();
  }
}
