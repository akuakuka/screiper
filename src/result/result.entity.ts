import { Keyword } from 'src/keywords/keywords.entity';
import { SourceEnum } from 'src/types/enums';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  ManyToOne,
} from 'typeorm';

@Entity()
export class Result extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  //TODO: Enum
  @Column({
    type: 'enum',
    enum: SourceEnum,
  })
  source: SourceEnum;

  @Column()
  sourceId: string;

  @Column({ nullable: true, type: 'bigint' })
  dateEpoch: number;

  @Column()
  url: string;

  @Column({ nullable: true })
  image: string;

  @Column({ nullable: true })
  price: string;

  @Column()
  subject: string;

  @Column()
  body: string;

  @Column({ default: false })
  notified: boolean;

  @ManyToOne(() => Keyword, (keyword) => keyword.results)
  keyword: Keyword;
}
